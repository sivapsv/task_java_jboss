# This is a jboss image

FROM jboss/wildfly

MAINTAINER s.v.siva.kumar.ponnaganti@ericsson.com

ARG deployable_artifact

COPY $deployable_artifact /opt/wildfly/standalone/deployments/

CMD ["echo", "JBoss image created successfully and war file was deployed successfully"]

